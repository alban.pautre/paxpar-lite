// plugins/readYaml.js
import fs from 'fs';
import path from 'path';
import yaml from 'js-yaml';

export default defineNuxtPlugin(() => {
    const readYaml = (file) => {
        const filePath = path.resolve(process.cwd(), 'server/craftforms', file);
        const fileContents = fs.readFileSync(filePath, 'utf8');
        return yaml.load(fileContents);
    };

    return {
        provide: {
        readYaml
        }
    };
});
