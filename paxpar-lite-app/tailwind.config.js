/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.vue",
    "./app.vue",
    "./node_modules/flowbite/**/*.js",
    "./server/api/*.(js,ts)",
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('flowbite/plugin'),
    require('daisyui'),
  ],
  darkMode: false
}

