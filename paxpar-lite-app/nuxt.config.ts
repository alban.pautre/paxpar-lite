// https://nuxt.com/docs/api/configuration/nuxt-config

import { defineNuxtModule, createResolver, addComponent } from '@nuxt/kit';
import { AppDashboard } from "#build/components";

export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxt/content",
    "@nuxt/ui",
    '@nuxtjs/tailwindcss',
  ],
  plugins: [
    "./plugins/readYaml.js",
  ],
  colorMode: {
    preference: 'light'
  },
})