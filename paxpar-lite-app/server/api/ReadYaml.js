// plugins/readYaml.js
import fs from 'fs'
import path from 'path'
import yaml from 'js-yaml'

export default ({ app }, inject) => {
const ReadYaml = (file) => {
    const filePath = path.resolve(__dirname, '..', 'path/to/your/yaml/files', file)
    const fileContents = fs.readFileSync(filePath, 'utf8')
    return yaml.load(fileContents)
}

inject('ReadYaml', readYaml)
}
