import fs from 'fs';
import path from 'path';
import yaml from 'js-yaml';
import { fileURLToPath } from 'url';

// Fonction pour lister les fichiers
const listFiles = (directory) => {
    let files = [];
    const entries = fs.readdirSync(directory, { withFileTypes: true });
    entries.forEach(entry => {
        const filePath = path.join(directory, entry.name);
        if (entry.isFile() && entry.name === 'base.craftform.yaml') {
            files.push(filePath);
        }
        if (entry.isDirectory()) {
            files = files.concat(listFiles(filePath));
        }
    });
    return files;
};

// Chemin du répertoire à explorer
const __filename = fileURLToPath(import.meta.url);
const rootDirectory = process.cwd();

// Appel de la fonction pour lister les fichiers
const files = listFiles(rootDirectory);
console.log(files)

export default defineEventHandler(() => {
    const forms = files.map(file => {
        const fileContents = fs.readFileSync(file, 'utf8');
        return yaml.load(fileContents);
    });

    return forms;
});
