import fs from 'fs';
import path from 'path';
import yaml from 'js-yaml';
import { defineEventHandler, getQuery, createError } from 'h3';
import { fileURLToPath } from 'url';

const getYamlSchema = (id) => {
    const __filename = fileURLToPath(import.meta.url);
    const rootDirectory = process.cwd();

    // Convertir l'ID en chemin de fichier
    const filePath = path.join(rootDirectory, 'server', 'craftforms', id.replace(/\./g, '/'), 'schema.yaml');
    console.log("rootDirectory",rootDirectory);
    console.log(`Checking path: ${filePath}`);

    if (fs.existsSync(filePath)) {
        console.log(`File found: ${filePath}`);
        const fileContents = fs.readFileSync(filePath, 'utf8');
        return yaml.load(fileContents);
    } else {
        console.log(`File not found: ${filePath}`);
        return null;
    }
};


export default defineEventHandler((event) => {
    console.log('Handling request for schema');
    const { id } = getQuery(event);
    console.log(`Received ID: ${id}`);
    const schema = getYamlSchema(id);

    if (schema) {
        console.log('Schema found, returning schema');
        return schema;
    } else {
        console.log('Schema not found, throwing error');
        throw createError({
            statusCode: 404,
            statusMessage: 'Schema not found',
        });
    }
});
